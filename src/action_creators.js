import * as actionTypes from './actions';
import axios from 'axios';


export function getUsers () {
	return dispatch => {
		axios.get('https://nuvi-challenge.herokuapp.com/activities')
			.then(res => {
				const {status, data} = res;
				if(status === 200) {
					dispatch({
						type: actionTypes.STOP_FETCHING,
					});
					dispatch({
						type: actionTypes.UPDATE_USER,
						Users: data.slice(0, 50)
					});
				}
			});
	}
}

export function startFetchingUsers () {
	return dispatch => {
		dispatch({
			type: actionTypes.START_FETCHING,
		});
	}
}

export function stopFetchingUsers() {
	return dispatch => {
		dispatch({
			type: actionTypes.STOP_FETCHING,
		});
	}
}
export function editData() {
	return dispatch =>{
		dispatch({
			type : actionTypes.EDIT,
		})
	}
}

export function saveData() {
	return dispatch => {
		dispatch({
			type:actionTypes.SAVE
		})
	}
}
export function pageChange(){
	return dispatch =>{
		dispatch({
			type : actionTypes.PAGE_CHANGE
		})
	}
} 
