import React ,{Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actionCreators from '../action_creators';
import ListView from '../components/ListView';
import Pagination from 'react-js-pagination';
import EditComponent from '../components/EditComponent';

export default class UserList extends Component {
	constructor(props) {
		super(props);
		this.get_Users = this.get_Users.bind(this)
		this.handlePageChange = this.handlePageChange.bind(this)
	}

	get_Users(){
		const { getUsers,startFetchingUsers } = this.props 
		startFetchingUsers();
		getUsers();
	}

	componentWillMount() {
		this.get_Users()
	}

	handlePageChange(pageNumber){
		const {pageChange} = this.props
		
		 console.log(`active page is ${pageNumber}`);
		  pageChange(pageNumber);
	}
	

	render(){
		const { Users,fetchUsers,editMode,activePage} = this.props
		let len = Users.length
		let user1 = Users.slice(0,10)
		let user2 = Users.slice(10,20)
		let user3 = Users.slice(20,30)
		let user4 = Users.slice(30,40)
		let user5 = Users.slice(40,len)
		switch(activePage){
						case 2 :
							return(<div className="row">
											{user2.length  ? user1.map((user,index)=>{
												return<ListView data={user} key={index}/>}) :null }
											{user2.length ? <Pagination
															          activePage={activePage}
															          itemsCountPerPage={10}
															          totalItemsCount={50}
															          pageRangeDisplayed={5}
															          onChange={::this.handlePageChange}
															          /> :null}
											</div>)
						case 3 :
							return(<div className="row">
											{user3.length  ? user1.map((user,index)=>{
												return<ListView data={user} key={index}/>}) :null }
											{user3.length ? <Pagination
															          activePage={activePage}
															          itemsCountPerPage={10}
															          totalItemsCount={50}
															          pageRangeDisplayed={5}
															          onChange={::this.handlePageChange}
															          /> :null}
											</div>)
						case 4 :
							return(<div className="row">
											{user4.length  ? user1.map((user,index)=>{
												return<ListView data={user} key={index}/>}) :null }
											{user4.length ? <Pagination
															          activePage={activePage}
															          itemsCountPerPage={10}
															          totalItemsCount={50}
															          pageRangeDisplayed={5}
															          onChange={::this.handlePageChange}
															          /> :null}
											</div>)
						case 5:
							return(<div className="row">
											{user5.length  ? user1.map((user,index)=>{
												return<ListView data={user} key={index}/>}) :null }
											{user5.length ? <Pagination
															          activePage={activePage}
															          itemsCountPerPage={10}
															          totalItemsCount={50}
															          pageRangeDisplayed={5}
															          onChange={::this.handlePageChange}
															          /> :null}
											</div>)
						default:
							return(<div className="row">
											{user1.length  ? user1.map((user,index)=>{
												return<ListView data={user} key={index}/>}) :null }
											{user1.length ? <Pagination
															          activePage={activePage}
															          itemsCountPerPage={10}
															          totalItemsCount={50}
															          pageRangeDisplayed={5}
															          onChange={::this.handlePageChange}
									          						   /> :null}
											</div>) }
	}
}

