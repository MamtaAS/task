import React from 'react';
import ReactDOM from 'react-dom';
import {List, Map} from 'immutable';
import {Provider} from 'react-redux';
import reducer from './reducer';
import {compose, createStore, applyMiddleware ,combineReducers} from 'redux';
import {Router, Route, browserHistory, IndexRoute ,Link} from 'react-router';
import thunk from 'redux-thunk';
import AppContainer from './components/App';
import createBrowserHistory from 'history/createBrowserHistory';
import EditContainer from './components/EditComponent'
import UserContainer from './container/UserList'
import Dummy from './components/Dummy'
import { syncHistoryWithStore, routerReducer } from 'react-router-redux'



const store = createStore(reducer,applyMiddleware(thunk));

//const history = syncHistoryWithStore(browserHistory, store)

store.dispatch({
	type: 'SET_STATE',
	state: {
		fetchUsers: false,
		Users: [],
		editMode :false,
		delMode :false,
		activePage : 0
	}
});

ReactDOM.render(
	<Provider store={store}>
		<Router history={browserHistory}>
			<Route path='/'component={AppContainer}>
				<IndexRoute component={UserContainer}/> 
				<Route path='/dummy'component={Dummy}/>
			</Route>
		</Router>
	</Provider>,document.getElementById('app'))
