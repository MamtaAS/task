import React,{Component} from 'react';
import UserList from '../container/UserList';
import EditComponent from './EditComponent';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actionCreators from '../action_creators';
import { Link} from 'react-router';


export default class ListView extends Component{
	constructor(props) {
		super(props);
		this.delData = this.delData.bind(this);
		this.editInfo = this.editInfo.bind(this);
	
	
	}

	delData(){
		let res = confirm("Do you want to delete?")
		if(res){
			console.log(`deleted user is ${this.props.data.actor_name}`)
		}
		else {
			console.log(`deletion is cancelled`)
		}
		
	}
	editInfo(){

		this.props.editData()
		/*this.props.editData()
			.then(res =>  this.props.router.replace('/dummy'))*/

	}
	

	render(){
		const {data ,editMode ,delMode,editData } = this.props
		return(<div className="col-lg-4 col-md-4 col-sm-8 col-xs-12 center-block " >
							<div className="activity"> 
								<img className="full-width no-padding " src={data.actor_avator} alt="image is not loaded"/>
								<label>NAME : </label>{data.actor_name}<br/>
								<label>DESRIPTION : </label>{data.actor_description}
		
								<div className="form-horizontal">
									<button className="btn btn-success" onClick={this.editData}>Edit</button> &nbsp;&nbsp;
									<button className="btn btn-success" onClick={this.delData}>Delete</button>
								</div>
							</div>	
						</div> )
	}
}  
