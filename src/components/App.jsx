import React,{Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actionCreators from '../action_creators';
import {Link} from 'react-router';
import UserList from '../container/UserList'

class App extends Component {
	constructor(props) {
		super(props);
		
	}

    render() {

        let children = React.cloneElement(this.props.children, {...this.props})
        return (
        	<div>
                {children}
            </div>

        );
    }
}

function mapStateToProps(state){
    return{
            fetchUsers : state.fetchUsers,
            Users : state.Users,
            editMode :state.editMode,
            delMode :state.delMode,
            activePage :1
    }
}

function mapDispatchToProps (dispatch) {
  return bindActionCreators(actionCreators, dispatch);
}

const AppContainer = connect(mapStateToProps, mapDispatchToProps)(App);

export default AppContainer
