import React ,{Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actionCreators from '../action_creators';


 export default class EditComponent extends Component {
	 constructor(props){
		super(props)
    this.saveInfo= this.saveInfo.bind(this);
    this.handleChange = this.handleChange.bind(this);
	}
  componentWillMount() {
    this.props.editData();
  }

  saveInfo(){
      this.props.saveData();
    }

  handleChange(e){
      let obj = {}
      obj[e.target.name] = e.target.value
      console.log(obj);
  }
	
	render(){
    const {name ,desc ,editMode } = this.props
		return(<div>
                {!editMode ?<div>
                              <div className="form-group">
                                  <label>Name </label>
                                  <input className="form-control"type="text"  name="username" value={name} onChange={this.handleChange}/>
                              </div><br/><br/>
                              <div className="form-group">
                                <label>Description</label>
                                  <input className="form-control" type="text"  name="description" value={desc} onChange={this.handleChange}/>
                              </div>
                               <div className="form-group">
                                  <button className="btn btn-success" type="submit" onClick={this.saveInfo} >Save</button>
                              </div>
                           </div> :null} 
                    </div> )
	                 }
}
