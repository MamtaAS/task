import {Map} from 'immutable';
import * as actionTypes from './actions';

function setState(state, newState) {
	return {...state, ...newState};
}

function updateUsers(state, Users) {
	return {...state, Users}
}


function updateData(state,User){
	const Updated_User = {}
	let {Users} = state.Users
	Users.push(User)
	return {...state,Users }

}

export default function (state = Map(), action) {
	switch(action.type) {
		case actionTypes.SET_STATE:
			return setState(state, action.state);
		case actionTypes.UPDATE_USER:
			return updateUsers(state, action.Users);
		case actionTypes.START_FETCHING:
			return {...state, fetchUsers: true};
		case actionTypes.STOP_FETCHING:
			return {...state, fetchUsers: false};
		case actionTypes.PAGE_CHANGE:
			return {...state};
		case actionTypes.EDIT:
			return {...state ,editMode :true};
		case actionTypes.SAVE :
			return {...state,editMode :false};
		case actionTypes.UPDATE_DATA :
			return updateData(state,action.User)
		default:
			return state
		}
	}	